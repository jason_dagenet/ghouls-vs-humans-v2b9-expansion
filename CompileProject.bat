@echo off
set ProjectName=GvH_V2B9_Expansion
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
	set dow=%%i
	set month=%%j
	set day=%%k
	set year=%%l
)
set version=r%month%%day%%year%
cd GvHv2b9_Expansion
..\Tools\7z a -tzip ..\%ProjectName%_%version%.pk3
pause