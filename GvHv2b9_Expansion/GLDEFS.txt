// Marine

object BulletPuff2
{
    frame PUFFA { light BPUFF1 }
    frame PUFFB { light BPUFF2 }
}

pointlight MARINESHOT
{
    color 0.8 0.8 0.0
    size 50
}

object DoomPlayer2
{
    frame PLAYF { light MARINESHOT }
}

flickerlight GRENADE_X1
{
    color 1.0 1.0 0.0
    size 80
    secondarySize 75
    chance 0.9
}

flickerlight GRENADE_X2
{
    color 1.0 1.0 0.0
    size 75
    secondarySize 65
    chance 0.9
}

flickerlight GRENADE_X3
{
    color 1.0 1.0 0.0
    size 65
    secondarySize 60
    chance 0.9
}

flickerlight GRENADE_X4
{
    color 1.0 1.0 0.0
    size 60
    secondarySize 55
    chance 0.9
}

flickerlight GRENADE_X5
{
    color 1.0 1.0 0.0
    size 55
    secondarySize 50
    chance 0.9
}


flickerlight GRENADE_X6
{
    color 1.0 1.0 0.0
    size 50
    secondarySize 45
    chance 0.9
}


flickerlight GRENADE_X7
{
    color 1.0 1.0 0.0
    size 45
    secondarySize 40
    chance 0.9
}

flickerlight GRENADE_X8
{
    color 1.0 1.0 0.0
    size 40
    secondarySize 35
    chance 0.9
}

flickerlight GRENADE_X9
{
    color 1.0 1.0 0.0
    size 35
    secondarySize 30
    chance 0.9
}

flickerlight GRENADE_X10
{
    color 1.0 1.0 0.0
    size 30
    secondarySize 25
    chance 0.9
}

flickerlight GRENADE_X11
{
    color 1.0 1.0 0.0
    size 25
    secondarySize 20
    chance 0.9
}

flickerlight GRENADE_X12
{
    color 1.0 1.0 0.0
    size 20
    secondarySize 15
    chance 0.9
}

flickerlight GRENADE_X13
{
    color 1.0 1.0 0.0
    size 15
    secondarySize 10
    chance 0.9
}

flickerlight GRENADE_X14
{
    color 1.0 1.0 0.0
    size 10
    secondarySize 5
    chance 0.9
}

flickerlight GRENADE_X15
{
    color 1.0 1.0 0.0
    size 5
    chance 0.9
}

object GrenadeExplosion
{
    frame NKXPB { light GRENADE_X1 }
	frame NKXPC { light GRENADE_X2 }
	frame NKXPD { light GRENADE_X3 }
	frame NKXPE { light GRENADE_X4 }
	frame NKXPF { light GRENADE_X5 }
	frame NKXPG { light GRENADE_X6 }
	frame NKXPH { light GRENADE_X7 }
	frame NKXPI { light GRENADE_X8 }
	frame NKXPJ { light GRENADE_X9 }
	frame NKXPK { light GRENADE_X10 }
	frame NKXPL { light GRENADE_X11 }
	frame NKXPM { light GRENADE_X12 }
	frame NKXPN { light GRENADE_X13 }
	frame NKXPO { light GRENADE_X14 }
	frame NKXPP { light GRENADE_X15 }
}

object Launchfx
{
    frame MNSMA { light GRENADE_X1 }
	frame MNSMB { light GRENADE_X2 }
	frame MNSMC { light GRENADE_X3 }
	frame MNSMD { light GRENADE_X4 }
	frame MNSME { light GRENADE_X5 }
	frame MNSMF { light GRENADE_X6 }
	frame MNSMG { light GRENADE_X7 }
	frame MNSMH { light GRENADE_X8 }
	frame MNSMI { light GRENADE_X9 }
	frame MNSMJ { light GRENADE_X10 }
	frame MNSMK { light GRENADE_X11 }
	frame MNSML { light GRENADE_X12 }
	frame MNSMM { light GRENADE_X13 }
	frame MNSMN { light GRENADE_X14 }
	frame MNSMO { light GRENADE_X15 }
}

// Hunter

pointlight FIREARROW
{
    color 1.0 0.0 0.0
    size 12
}

pointlight FIREARROWX1
{
    color 1.0 0.2 0.0
    size 30
}

pointlight FIREARROWX2
{
    color 1.0 0.2 0.0
    size 25
}

pointlight FIREARROWX3
{
    color 1.0 0.2 0.0
    size 20
}

pointlight FIREARROWX4
{
    color 1.0 0.2 0.0
    size 15
}

pointlight FIREARROWX5
{
    color 1.0 0.2 0.0
    size 10
}

pointlight FIREARROWX6
{
    color 1.0 0.2 0.0
    size 5
}

object FlameArrow
{
    frame FARW { light FIREARROW }
	frame HELXA { light FIREARROWX1 }
	frame HELXB { light FIREARROWX2 }
	frame HELXC { light FIREARROWX3 }
	frame HELXD { light FIREARROWX4 }
	frame HELXE { light FIREARROWX5 }
	frame HELXF { light FIREARROWX6 }
}

pointlight ICEARROW
{
    color 0.0 0.0 1.0
    size 12
}

pointlight ICEARROWX1
{
    color 0.0 0.0 1.0
    size 30
}

pointlight ICEARROWX2
{
    color 0.0 0.0 1.0
    size 20
}

pointlight ICEARROWX3
{
    color 0.0 0.0 1.0
    size 10
}

object IceArrow
{
	frame SHRD { light ICEARROW }
	frame SHEXA { light ICEARROWX1 }
	frame SHEXB { light ICEARROWX2 }
	frame SHEXC { light ICEARROWX3 }
}

object IceArrow2
{
	frame SHRD { light ICEARROW }
	frame SHEXA { light ICEARROWX1 }
	frame SHEXB { light ICEARROWX2 }
	frame SHEXC { light ICEARROWX3 }
}

pointlight ARROWHIGHLIGHT
{
    color 0.3 0.3 0.3
    size 8
}

object ArrowDropped
{
	frame ARRW { light ARROWHIGHLIGHT }
}

flickerlight FIRERADIUS1
{
	color 1.0 0.0 0.0
    size 100
    secondarySize 80
}

flickerlight FIRERADIUS2
{
	color 1.0 0.0 0.0
    size 80
    secondarySize 60
}

flickerlight FIRERADIUS3
{
	color 1.0 0.0 0.0
    size 60
    secondarySize 40
}

flickerlight FIRERADIUS4
{
	color 1.0 0.0 0.0
    size 40
    secondarySize 20
}

flickerlight FIRERADIUS5
{
    color 1.0 0.0 0.0
    size 20
}

object FireRadius
{
	frame TNT1A { light FIRERADIUS1 }
	frame TNT1B { light FIRERADIUS2 }
	frame TNT1C { light FIRERADIUS3 }
	frame TNT1D { light FIRERADIUS4 }
	frame TNT1E { light FIRERADIUS5 }
}

flickerlight ICERADIUS1
{
	color 0.0 0.0 1.0
    size 100
    secondarySize 80
}

flickerlight ICERADIUS2
{
	color 0.0 0.0 1.0
    size 80
    secondarySize 60
}

flickerlight ICERADIUS3
{
	color 0.0 0.0 1.0
    size 60
    secondarySize 40
}

flickerlight ICERADIUS4
{
	color 0.0 0.0 1.0
    size 40
    secondarySize 20
}

flickerlight ICERADIUS5
{
    color 0.0 0.0 1.0
    size 20
}

object IceRadius
{
	frame TNT1A { light ICERADIUS1 }
	frame TNT1B { light ICERADIUS2 }
	frame TNT1C { light ICERADIUS3 }
	frame TNT1D { light ICERADIUS4 }
	frame TNT1E { light ICERADIUS5 }
}

flickerlight LIGHTNINGRADIUS1
{
	color 1.0 1.0 0.0
    size 100
    secondarySize 80
}

flickerlight LIGHTNINGRADIUS2
{
	color 1.0 1.0 0.0
    size 80
    secondarySize 60
}

flickerlight LIGHTNINGRADIUS3
{
	color 1.0 1.0 0.0
    size 60
    secondarySize 40
}

flickerlight LIGHTNINGRADIUS4
{
	color 1.0 1.0 0.0
    size 40
    secondarySize 20
}

flickerlight LIGHTNINGRADIUS5
{
    color 1.0 1.0 0.0
    size 20
}

object LightningRadius
{
	frame TNT1A { light LIGHTNINGRADIUS1 }
	frame TNT1B { light LIGHTNINGRADIUS2 }
	frame TNT1C { light LIGHTNINGRADIUS3 }
	frame TNT1D { light LIGHTNINGRADIUS4 }
	frame TNT1E { light LIGHTNINGRADIUS5 }
}

object LightningFall
{
	frame FX16G { light LIGHTNINGRADIUS1 }
	frame FX16H { light LIGHTNINGRADIUS2 }
	frame FX16I { light LIGHTNINGRADIUS3 }
	frame FX16J { light LIGHTNINGRADIUS4 }
	frame FX16K { light LIGHTNINGRADIUS5 }
}

flickerlight NORMALRADIUS1
{
	color 1.0 1.0 1.0
    size 100
    secondarySize 80
}

flickerlight NORMALRADIUS2
{
	color 1.0 1.0 1.0
    size 80
    secondarySize 60
}

flickerlight NORMALRADIUS3
{
	color 1.0 1.0 1.0
    size 60
    secondarySize 40
}

flickerlight NORMALRADIUS4
{
	color 1.0 1.0 1.0
    size 40
    secondarySize 20
}

flickerlight NORMALRADIUS5
{
    color 1.0 1.0 1.0
    size 20
}

object NormalRadius
{
	frame TNT1A { light NORMALRADIUS1 }
	frame TNT1B { light NORMALRADIUS2 }
	frame TNT1C { light NORMALRADIUS3 }
	frame TNT1D { light NORMALRADIUS4 }
	frame TNT1E { light NORMALRADIUS5 }
}

// Cyborg

pointlight PLASMABALL
{
    color 0.0 0.1 1.0
    size 56
}

flickerlight PLASMA_X1
{
    color 0.2 0.2 1.0
    size 64
    secondarySize 72
    chance 0.4
}

flickerlight PLASMA_X2
{
    color 0.2 0.2 0.8
    size 80
    secondarySize 88
    chance 0.4
}

flickerlight PLASMA_X3
{
    color 0.1 0.1 0.5
    size 64
    secondarySize 72
    chance 0.4
}

flickerlight PLASMA_X4
{
    color 0.0 0.0 0.2
    size 8
    secondarySize 16
    chance 0.4
}

pointlight JETPACK1
{
    color 1.0 1.0 0.0
    size 33
}

pointlight JETPACK2
{
    color 1.0 0.5 0.0
    size 30
}

pointlight JETPACK3
{
    color 1.0 0.8 0.0
    size 39
}

object Firey
{
    frame BFIRA { light JETPACK1 }
    frame BFIRB { light JETPACK2 }
	frame BFIRC { light JETPACK3 }
}

object CyborgPlasmaBall
{
    frame SBLLA { light PLASMABALL }
    frame SBLLB { light PLASMABALL }
	frame SBLLC { light PLASMABALL }
	frame SBLLD { light PLASMABALL }
	frame SBLLE { light PLASMABALL }
	frame SBLLF { light PLASMABALL }
	frame SBLLG { light PLASMABALL }
	frame SBLLH { light PLASMABALL }
	frame SBLLI { light PLASMABALL }
	frame SBLLJ { light PLASMABALL }

    frame SBLLK { light PLASMA_X1 }
    frame SBLLL { light PLASMA_X2 }
    frame SBLLM { light PLASMA_X2 }
    frame SBLLN { light PLASMA_X3 }
    frame SBLLO { light PLASMA_X4 }
}

// Global
flickerlight ARCHFIRE1
{
    color 1.0 1.0 0.0
    size 24
    secondarySize 32
    chance 0.3
    offset 0 8 0
}

flickerlight ARCHFIRE2
{
    color 1.0 1.0 0.0
    size 40
    secondarySize 48
    chance 0.3
    offset 0 24 0
}

flickerlight ARCHFIRE3
{
    color 1.0 1.0 0.0
    size 64
    secondarySize 72
    chance 0.3
    offset 0 32 0
}

flickerlight ARCHFIRE4
{
    color 0.8 0.8 0.0
    size 64
    secondarySize 72
    chance 0.3
    offset 0 40 0
}

flickerlight ARCHFIRE5
{
    color 0.8 0.8 0.0
    size 64
    secondarySize 72
    chance 0.3
    offset 0 48 0
}

flickerlight ARCHFIRE6
{
    color 0.6 0.6 0.0
    size 48
    secondarySize 56
    chance 0.3
    offset 0 64 0
}

flickerlight ARCHFIRE7
{
    color 0.4 0.4 0.0
    size 32
    secondarySize 40
    chance 0.3
    offset 0 72 0
}

flickerlight ARCHFIRE8
{
    color 0.2 0.2 0.0
    size 16
    secondarySize 24
    chance 0.3
    offset 0 80 0
}

object ArchvileFirex
{
    frame FIREA { light ARCHFIRE1 }
    frame FIREB { light ARCHFIRE2 }
    frame FIREC { light ARCHFIRE3 }
    frame FIRED { light ARCHFIRE4 }
    frame FIREE { light ARCHFIRE5 }
    frame FIREF { light ARCHFIRE6 }
    frame FIREG { light ARCHFIRE7 }
    frame FIREH { light ARCHFIRE8 }
}

object ArchvileFirex2
{
    frame FIREA { light ARCHFIRE1 }
    frame FIREB { light ARCHFIRE2 }
    frame FIREC { light ARCHFIRE3 }
    frame FIRED { light ARCHFIRE4 }
    frame FIREE { light ARCHFIRE5 }
    frame FIREF { light ARCHFIRE6 }
    frame FIREG { light ARCHFIRE7 }
    frame FIREH { light ARCHFIRE8 }
}

// GVH01 Stuff

flickerlight BIGLIGHT
{
    color 0.5 0.5 0.0
    size 400
    secondarySize 500
    chance 0.8
}

object Logfire
{
    frame TNT1A { light BIGLIGHT }
}

brightmap sprite JSKLA0
{
	map "sprites/classes/JSKBA0"
}

brightmap sprite JSKLB0
{
	map "sprites/classes/JSKBB0"
}

brightmap sprite JSKLC0
{
	map "sprites/classes/JSKBC0"
}

pointlight GhostTrap
{
    color 0.0 0.0 0.8
    size 16
}

object GhostTrap
{
    frame GTRPE0 { light GhostTrap }
}

flickerlight TRAP_X1
{
    color 0.0 0.0 1.0
    size 80
    secondarySize 75
    chance 0.5
}

flickerlight TRAP_X2
{
    color 0.3 0.3 1.0
    size 75
    secondarySize 65
    chance 0.5
}

object GhostTrapFX
{
    frame GTRPB0 { light TRAP_X1 }
	frame GTRPC0 { light TRAP_X2 }
	frame GTRPD0 { light TRAP_X1 }
}

pointlight ProtonBall
{
    color 1.0 0.0 0.0
    size 30
}

pointlight ProtonBall2
{
    color 0.0 0.0 1.0
    size 40
}

flickerlight Proton_X1
{
    color 1.0 0.0 0.0
    size 30
    secondarySize 40
    chance 0.4
}

flickerlight Proton_X2
{
    color 1.0 0.2 0.2
    size 20
    secondarySize 30
    chance 0.4
}

flickerlight Proton_X3
{
    color 1.0 0.3 0.3
    size 10
    secondarySize 20
    chance 0.4
}

flickerlight Proton_X4
{
    color 1.0 0.4 0.4
    size 5
    secondarySize 10
    chance 0.4
}

object GhostBusterBall
{
    frame PROTA0 { light ProtonBall }
    frame PROTB0 { light ProtonBall2 }

    frame PROTE0 { light Proton_X1 }
	frame PROTE0 { light Proton_X1 }
    frame PROTF0 { light Proton_X2 }
    frame PROTG0 { light Proton_X2 }
    frame PROTH0 { light Proton_X3 }
    frame PROTI0 { light Proton_X4 }
}

//object FrozenWind
//{
//	frame PLSSJ { light ICERADIUS1X }
//	frame PLSSK { light ICERADIUS1X }
//	frame PLSSL { light ICERADIUS1X }
//	frame PLSSM { light ICERADIUS1X }
//	frame PLSSN { light ICERADIUS1X }
//	frame PLSSO { light ICERADIUS1X }
//	frame PLSSP { light ICERADIUS1X }
//	frame PLSSQ { light ICERADIUS1X }
//	frame PLSSR { light ICERADIUS1X }
//	frame PLSSS { light ICERADIUS1X }
//	frame PLSST { light ICERADIUS1X }
//	frame PLSSU { light ICERADIUS2X }
//	frame PLSSV { light ICERADIUS3X }
//	frame PLSSW { light ICERADIUS4X }
//	frame PLSSX { light ICERADIUS5X }
//}

object FrostDeathFX
{
	frame PLSSJ { light ICERADIUS1X }
	frame PLSSK { light ICERADIUS1X }
	frame PLSSL { light ICERADIUS1X }
	frame PLSSM { light ICERADIUS1X }
	frame PLSSN { light ICERADIUS1X }
	frame PLSSO { light ICERADIUS1X }
	frame PLSSP { light ICERADIUS1X }
	frame PLSSQ { light ICERADIUS1X }
	frame PLSSR { light ICERADIUS1X }
	frame PLSSS { light ICERADIUS1X }
	frame PLSST { light ICERADIUS1X }
	frame PLSSU { light ICERADIUS2X }
	frame PLSSV { light ICERADIUS3X }
	frame PLSSW { light ICERADIUS4X }
	frame PLSSX { light ICERADIUS5X }
}

pointlight ICERADIUS1X
{
	color 0.0 0.0 1.0
    size 100
}

pointlight ICERADIUS2X
{
	color 0.0 0.0 1.0
    size 80
}

pointlight ICERADIUS3X
{
	color 0.0 0.0 1.0
    size 60
}

pointlight ICERADIUS4X
{
	color 0.0 0.0 1.0
    size 40
}

pointlight ICERADIUS5X
{
    color 0.0 0.0 1.0
    size 20
}

object SteamProjectile
{
    frame EMBPA { light ARCHFIRE1 }
    frame EMBPB { light ARCHFIRE2 }
    frame EMBPC { light ARCHFIRE3 }
    frame EMBPD { light ARCHFIRE4 }
    frame EMBPE { light ARCHFIRE5 }
}