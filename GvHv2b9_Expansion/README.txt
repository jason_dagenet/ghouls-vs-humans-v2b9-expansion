.Best viewed in SLumpEd or notepad!
                                                                              
     -o:`                                                                       
      /NN/                ./+oosoo/:`                                           
      `mNNy. :`        /shhhddddyhhdmdo:`                                       
      `myddh+hh`      shddddddddhyhdddMMm+`                                     
     -omydddyyms    .ohddddddyhddhhhdhmMMNh-                                    
    -hhhhhhdhyNy    -sydshdhyhddddhhdsmNNNMN-                                   
   `sddddhhddmMs    - .y+hhyyddhhh+hdyMMMMNM:                                   
   .hddddyhddMs`   :`  -/dysyhyo+/+yhsMNdhmd-                                   
    oh/sdhhdmo    `+    /o/:-.`    yhhdyoyhs                                    
    :- `sdhdN`    `s/../ss+`      .yddd+sdh-                                    
        +dhdm.     `syhhddhy+:-::/shddh+yho`                                    
        +ddhdy      /hysyyyddhshyyyyddhyydMmy+o/-`                              
        /hdhdd+     -ooymmo+yhosydddyhdhddNMMMMMNms-                            
        `-ydddd/    `/+/o+/+++shdddoshh+sdmmNMMMMMMNd+-                         
          .ydddd+   `+:sy+/////ddmosh+ds+odMmMMMMMMMMMmh:                       
           .hdddho` :y+/+//////dNosyh/ym++hNMMMMMMMMMMNmms-                     
            +dddddy:/y+/+++/+ohdd/hyd+oMssMMMNNNMMMMMMMMNmdy.                   
            .ddddddhyyh+++++yyyhs+syNyhMNNMNdddmMMMMMMNMMMdm`                   
            .ddddddddhdhy++/yyhs++oyNdMMNmhhmNNNNNmMMNdNMMdMo.``        `..     
            `:oddddhhddddhyyyddhs++yNMNdhyhmdhssshMMmddNMMhMMNddhso+:-` ---` `  
          `...:ddddhhdddddy/.sdddyyhNmhyhsooooyhdmNddddNMMydNMMMMMMMMNmy+.      
         `oyyhdddddddddddh`  .ydddyddhshyssssssssyyhhddNMMyddmMMMMMMMNho/-      
           `.-/+osyyyyhhdd+.  .yhdhdddhyyhhhdddhyyhhhddMMhhddddNMMMMMh.    -/-  
                  ````..://:  :yyhhdddddddddddhyhdhhhmMMhydddddddmmNMMNy/.      
                              `/yhyhddddddddhyydmhhdmMMdyhyhhddddddmNMN:..      
                  ./sys:`   /syyhddhddddddhhyhdddddmMMddy:`.-:///////++/`       
               `:shdNMMMds-`./oyyhhhhhhhhhyhddddddmsNdmmdy.                     
              .sddddMMMMMMMd+.  ``-----:shddddddh+//:::-.                       
              hdddddddMMMNmNMNh:` ```/shdddddddh:-:`                            
              ddddddddNNNNdddmmNmhhydddddmdddddds+`                             
              sdddddddm.:hddddddddddddddddddddyds-                              
            ./yddddddmMmmNdddddddddddddddddddddy`                               
            .//+osyydMMMmddddmmmmmmmmmmmmmmmdyso.                               
                  -/sNMMMMMMMMMMMMMMMMMMMMMo                                    
                  oydmNMMMMMMMMMMMd+:::/hMMNo`  ::                              
                    `-/+yydmNNNNMMNmdhhmMMNNho                                  
                            ``....-:////:-.                                     

  ____  _                    _                     _   _                                      
 / ___|| |__    ___   _   _ | | ___  __   __ ___  | | | | _   _  _ __ ___    __ _  _ __   ___ 
| |  _ | '_ \  / _ \ | | | || |/ __| \ \ / // __| | |_| || | | || '_ ` _ \  / _` || '_ \ / __|
| |_| || | | || (_) || |_| || |\__ \  \ V / \__ \ |  _  || |_| || | | | | || (_| || | | |\__ \
 \____||_| |_| \___/  \__,_||_||___/   \_/  |___/ |_| |_| \__,_||_| |_| |_| \__,_||_| |_||___/

GVH Version 2 beta 8 (FOR USE WITH Skulltag 98a and higher)
==============================================================================================
==============================================================================================

********************
*INTRODUCTION      *
********************

* NOT FOR THE FAINT OF HEART! *

The Ghoul's Forest is a series created by Me, CutmanMike. Rather than you're old run
 'n gun adventures on regular doom, the Ghoul series put your player in a dark forest, usually
with very limited ammo and a single tough, scary monster to kill. For Doom fans it was hit or 
miss, but regardless original The Ghoul's Forest spawned two sequels, many fan created WADs 
and videos, and now Ghouls versus Humans: A team based class deathmatch WAD that brings the 
lovable creatures from the Ghoul series to the arena!

Players are put into two teams. Humans and Ghouls. Each team has different classes to choose 
from. Humans have the Marine, Hunter and Cyborg to choose from, while the Ghoul side has Sjas,
 The Creeper and Jitterskull. Each class has unique attacks and abilities, pros and cons. 
There are no items, weapons, powerups or anything else to rely on. Just you, the other players
 and the map.
 
Ghouls vs Humans v2 merges all the classes and maps from the expansions, making it easier to
set up a game and get fragging.

Visit http://cutstuff.net for more info, stratergies and more!

********************
*GAMEPLAY MODES    *
********************

Ghouls versus Humans should be compatable with all of SkullTag's gameplay modes, but it is 
designed for Team Deathmatch and Team Last Man Standing. Classes have been balanced for these
 modes and should be played in team games. After all, it isn't called "Ghouls vs Humans" for 
nothing is it?

---------------
SERVER SETTINGS
---------------

Your options (DMFLAGS etc) should naturally be up to the server, but GVH has some flags that are 
required to be off or on in order to play properly.

DMFLAGS = 2622592
DMFLAGS2 = 0
COMPATFLAGS = 0

BE SURE to enable the following settings as they can have drastic effects on the game:

compat_noland = 1
compat_clientssendfullbuttoninfo = 1

The following is optional.

sv_useteamstartsindm - Must be enabled via the command line. This forces players to spawn at their
team starts (set by the map author), and swaps spawning variety for zero spawn killing.

********************
*BOT SUPPORT       *
********************

This wad contains various bots you can use for offline or online bot matches. Keep in mind that 
 they need to be assigned to the right teams or they won't spawn as their prefered class.

If you're planning to use bots in team matches, use the compatflag "Player keeps his team 
after map change" (8192).

********************
*ADDITIONAL CREDITS*
********************

----------
Testers
----------

Firewolf
Wario
Zippy
Icy
KeksDose
Snarboo
Eriance
XutaWoo
Wills
_Dark_
DevilHunter
Zorcher
Hetor

---------
Audio
---------

Bouncy
Eriance
LittleWhiteMouse
DJ-Roulette
XxX-WOLF-XxX
DavidOrr
Jonathan Bradley (hereforrock)
DarKsidE555
Wolfguard

Note: Song titles etc can be found in music/LICENSING.txt

---------
Artwork
---------

FuPoo

---------
Graphics
---------

Id Software
Raven Software
Eriance
Rouge Entertainment
cool-text.com
Cabal Online
Nash
Kinsie
Marty Kirra
Esselfortium
Paul(Nmn)
ClonedPickle
MidoriMan
Epic Games

---------
Coders
---------

HotWax
Nash
Kinsie
carlcyber

---------
Special Thanks
---------

Randy Heit
Carnevil
Graf Zahl
Torr Samaho
Newgrounds.com

*** IF YOU BELIEVE YOU DESERVE CREDIT FOR SOMETHING E-MAIL ME ***

********************
*CONTACT		  *
********************
Website: http://www.cutstuff.net

If you wish to contact me, give me feedback, hatemail, whatever, drop me a line!

mikehill2005@gmail.com

********************
*PERMISSIONS       *
********************

Don't steal anything without permission.
If you want to use anything PLEASE CONTACT ME FIRST!!!

==============================================================================================
==============================================================================================