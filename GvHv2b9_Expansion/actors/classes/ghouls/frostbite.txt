//XMAS CLASSES

actor FrostBite : PlayerPawn
{
    Health 150
    Mass 9000
    limitedtoteam 1
    player.forwardmove 0.85
    player.sidemove 0.85
    Player.Maxhealth 150
    Player.DisplayName "FrostBite"
	bloodcolor "Blue"
    player.startitem "FrostTeeth"
	player.startitem "FrostAmmo", 50
	player.startitem "BlueArmor2", 1
    player.crouchsprite "FROB"
    player.soundclass "frost"
	player.startitem "AchievementID", 2
	//player.jumpz 7
	bloodtype "IceChunkFB3"
    Player.ColorRange 112, 127
    +NOSKIN
	//+BRIGHT
	gravity 0.9
	renderstyle translucent
	player.scoreicon "FROSTST"
    scale 0.24
    +NOICEDEATH
	+QUICKTORETALIATE
    States
    {
    Spawn:
    TNT1 A 0    
    FROB A 1 //A_SetTranslucent(0.50, 0)
    Goto Spawn+1
    See:
    FROB A 2 A_SetTranslucent(0.50, 0)
    FROB A 2 A_SetTranslucent(0.55, 0)
    FROB A 2 A_SetTranslucent(0.60, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.65, 0)
    FROB A 2 A_SetTranslucent(0.70, 0)
    FROB A 2 A_SetTranslucent(0.75, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.80, 0)
    FROB A 2 A_SetTranslucent(0.85, 0)
    FROB A 2 A_SetTranslucent(0.90, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.95, 0)
    FROB A 2 A_SetTranslucent(1.0, 0)
    FROB A 2 A_SetTranslucent(0.95, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.90, 0)
    FROB A 2 A_SetTranslucent(0.85, 0)
    FROB A 2 A_SetTranslucent(0.80, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.75, 0)
    FROB A 2 A_SetTranslucent(0.70, 0)
    FROB A 2 A_SetTranslucent(0.65, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    FROB A 2 A_SetTranslucent(0.60, 0)
    FROB A 2 A_SetTranslucent(0.55, 0)
    FROB A 2 A_SetTranslucent(0.50, 0)
	FROB A 0 A_SpawnItemEx("FrostTwinkle", 0, random(-40,40), random(-40,90),0,0,0,0,32)
    Goto Spawn+1
    Missile:
	FROB A 0 A_JumpIfInventory("Clip",1,"Breath")
    FROB A 0 A_SetTranslucent(1.0, 0)
    FROB BBBB 1 A_JumpIfInventory("Clip",1,"Breath")
	FROB CD 4
    FROB D 0 //A_CustomMissile("IceShat")
    FROB EFEA 3
    Goto Spawn+1
	Breath:
	FROB D 18 bright
	FROB CB 4 bright
	Goto Spawn
    Death:
    DeathAnim:
	SANT A 0 A_GiveToTarget("KilledMe",1)
	SANT A 0 A_GiveToTarget("KilledFrost",1)
	FROB A 0 A_GiveInventory("GhoulAmmo",1)
	FROB A 0 A_PlaySoundEx("FrostDeath", "Body")
	FROB A 0 A_Fall
	FROB A 0 A_SpawnItemEx("LargeBurst",0,0,0,0,0,0,0,32)
	FROB ZZZZZZ 6 A_SpawnItemEx("FrostDeathFX",0,0,32,1,0,2,random(0,360),32)
    FROB A 0 //A_GiveToTarget("KilledFrost",1)
    FROB Z -1
    Stop
	Death.GBTrap:
	Goto Death.Grenade
    Death.Grenade:
    FROB A 0 A_GiveToTarget("KilledG",1)
    Goto DeathAnim
    Death.CrapPlasma:
    FROB A 0 A_GiveToTarget("Unlock9",1)
    Goto DeathAnim
    Death.Creeper:
    FROB A 0 A_PlaySoundEx("creeperattack", "Body")
    FROB A 0 ACS_ExecuteAlways(997,0)
    FROB A 0 A_GiveToTarget("Creeperheal",1)
    Goto DeathAnim
    Death.ice2:
    FROB A 0 A_GiveToTarget("Killedice2",1)
    Goto DeathAnim
    Death.fire2:
    FROB A 0 A_GiveToTarget("Killedfire2",1)
    Goto DeathAnim
    Death.Railgun:
    FROB A 0 A_GiveToTarget("Killedlightning",1)
    Goto DeathAnim
    }
}

actor BlueArmor2 : BlueArmor
{
inventory.icon "FROZS"
}

actor KilledFrost : KilledSjas
{
}


Actor FrostDeathFX
{
-SOLID
+CLIENTSIDEONLY
+NOGRAVITY
renderstyle add
alpha 0.9
scale 2.5
States
{
Spawn:
PLSS JKLMNOP 5 A_FadeOut(0.05)
PLSS QRSTUVWX 4 A_FadeOut(0.05)
stop
}
}


Actor LargeBurst
{
Radius 40
Height 80
- SOLID
+NOGRAVITY
States
{
Spawn:
TNT1 A 0
TNT1 A 0 A_PlaySound("FrostBurst")
TNT1 A 1 A_Burst("IceChunkFB2") 
stop
}
}

Actor IceChunkFB
{
Radius 3
Height 4
+CLIENTSIDEONLY
- SOLID
-NOGRAVITY
States
{
Spawn:
ICEC A 0
ICEC A 0 A_Recoil(-5)//ThrustThingZ(0,random(1,15),0,0)
ICEC AAAABCD 3
Stop
}
}

Actor IceChunkFB2
{
Radius 3
Height 4
+CLIENTSIDEONLY
- SOLID
+LOWGRAVITY
-NOGRAVITY
States
{
Spawn:
ICEC A 0
ICEC A 0 ThrustThingZ(0,random(1,15),0,0)
ICEC ABCD 30
Stop
}
}

Actor IceChunkFB3
{
Radius 3
Height 4
+CLIENTSIDEONLY
- SOLID
-NOGRAVITY
States
{
Spawn:
ICEC A 0
ICEC A 0 ThrustThingZ(0,random(1,15),0,0)
ICEC A 0 ThrustThing(random(0,256),random(1,8))
ICEC ABCD 4
Stop
}
}


actor FrostTwinkle
{
+NOINTERACTION
+CLIENTSIDEONLY
Radius 3
Height 3
+NOGRAVITY
renderstyle add
- SOLID
States
{
Spawn:
TGLT F 0 bright
TGLT F 0 bright A_PlaySound("FrostTwinkle")
TGLT FGHIFGHIFGHIFGHI 2 bright A_FadeOut(0.02)
stop
}
}

Actor FrostAmmo : Ammo
{
inventory.amount 50
inventory.maxamount 50
inventory.icon "FROSAM"
}

Actor FrostTeeth : Weapon
{
+WEAPON.DONTBOB
   Weapon.SelectionOrder 350
   Inventory.PickupSound ""
   Inventory.PickupMessage ""
   Weapon.SlotNumber 1
   Weapon.AmmoType "FrostAmmo"
   Weapon.AmmoGive 1
   Weapon.AmmoUse 1
   +WEAPON.MELEEWEAPON
   Weapon.Kickback 0
   Weapon.YAdjust 0
   +INVENTORY.UNDROPPABLE
   +WEAPON.ALT_AMMO_OPTIONAL
   inventory.icon "FROSTATT"
   States
   {
   Spawn:
      FROB V 1
      Stop
   Ready:
      FROB VVVVVVVVVVVVVVVVVVV 1 A_WeaponReady
	  FROB VVVVVVV 1 A_WeaponReady
      FROB V 0 A_GiveInventory("FrostAmmo",1)
	  FROB V 0 A_GiveInventory("ArmorBonus",1)
      Goto Ready+19
   Deselect:
      FROB V 0 A_StopSoundEx("Weapon")
      FROB V 1 A_Lower
      Loop
   Select:
      FROB V 1 A_Raise
      Goto Select
   Fire:
     FROB V 0 A_Playweaponsound("FrostChomp")
	 FROB V 0 A_Recoil(-20)
     FROB V 2 offset (0, 55)
     FROB V 2 offset (0, 65)
     FROB V 2 offset (0, 75)
     FROB V 2 offset (0, 95)
     FROB V 2 offset (0, 105)
     FROB V 2 offset (0, 135)
	 FROB V 0 A_Stop
	 FROB V 0 A_SpawnItemEx("IceChunkFB", 0, random(-22,22), random(2,15), momx, momy, random(8,12), random(-8,8),40)
	 FROB V 0 A_SpawnItemEx("IceChunkFB", 0, random(-22,22), random(2,15), momx, momy, random(8,12), random(-8,8),40)
	 FROB V 0 A_SpawnItemEx("IceChunkFB", 0, random(-22,22), random(2,15), momx, momy, random(8,12), random(-8,8),40)
	 FROB V 0 A_SpawnItemEx("IceChunkFB", 0, random(-22,22), random(2,15), momx, momy, random(8,12), random(-8,8),40)
	 FROB V 0 A_JumpIfInventory("Crunched",1,"Take")
	 FROB V 0 A_Playweaponsound("FrostTeeth")
     FROB V 3 offset (0, 38) A_FireCustomMissile("FrostAttack", 0, 0 ,0)
     FROB V 3 offset (0, 29)
     FROB V 3 offset (0, 34)
	 FROB V 3 
	 FROB V 3 A_Refire
	 FROB V 0 A_TakeInventory("Crunched",999)
     goto Ready
   Take:
     FROB V 0 A_TakeInventory("Crunched",999)
	 Goto Fire+14
   Altfire:
     FROB V 0 A_GiveInventory("Clip",1)
     FROB V 0 A_PlaySoundEx("FrostBreathStart","Weapon")
     FROB V 3 offset (0, 55)
     FROB V 3 offset (0, 75)
     FROB V 3 offset (0, 105)
     FROB V 4 offset (0, 135)
     FROB V 0 A_JumpIfInventory("FrostAmmo",1,"Return")
     FROB V 4 offset (0, 135)
     FROB V 0 A_Refire
	 FROB V 0 A_TakeInventory("KilledJ",999)
	 FROB V 0 A_StopSoundEx("Weapon")
	 FROB V 0 A_PlaySoundEx("FrostBreathEnd","Weapon")
     FROB V 1 offset (0, 135)
     FROB V 1 offset (0, 105)
     FROB V 1 offset (0, 75)
     FROB V 1 offset (0, 55)
     FROB V 0 offset (0, 34)
	 FROB V 0 A_TakeInventory("Clip",1)
     Goto Ready
  AltHold:
     FROB V 0 A_GiveInventory("RocketAmmo",1)
	 FROB V 0 A_JumpIfInventory("RocketAmmo",5,"BreathSound")
     FROB V 0 A_JumpIfInventory("FrostAmmo",1,"AltReturn")
	 FROB V 0 A_StopSoundEx("Weapon")
	 FROB V 16 A_PlaySoundEx("FrostBreathFail","Weapon")
     FROB V 4 offset (0, 135) 
     FROB V 0 A_Refire
	 FROB V 0 A_TakeInventory("KilledJ",999)
	 FROB V 0 A_StopSoundEx("Weapon")
	 FROB V 0 A_PlaySoundEx("FrostBreathEnd","Weapon")
     FROB V 1 offset (0, 135)
     FROB V 1 offset (0, 105)
     FROB V 1 offset (0, 75)
     FROB V 1 offset (0, 55)
     FROB V 0 offset (0, 34) //A_ClearRefire
	 FROB V 0 A_TakeInventory("Clip",1)
     Goto Ready
  Return:
     FROB V 0 //A_PlaySoundEx("FrostBreathMid", "Weapon")
	 FROB V 0 offset (0, 135) A_FireCustomMissile("FrozenWind",random(-15,15),1,0)
	 Goto Altfire+7
  BreathSound:
     FROB V 0 A_PlaySoundEx("FrostBreathMid", "Weapon")
	 FROB V 0 A_TakeInventory("RocketAmmo",5)
	 Goto AltHold+2
  AltReturn:
     FROB V 0 offset (0, 135) A_FireCustomMissile("FrozenWind",random(-15,15),0,0)
	 FROB V 0 A_TakeInventory("FrostAmmo",1)
	 Goto AltHold+5
   }
}


Actor FrostAttack
{
//PROJECTILE
+NOGRAVITY
+THRUGHOST
+FORCERADIUSDMG
Speed 10
Radius 2
Height 2
Obituary "%o got frost bite from %k."
damagetype "Jitter"
damage 0
+RIPPER
States
{
Spawn:
TNT1 A 0
TNT1 A 1
Goto Death
Death:
TNT1 A 1 A_Explode(99, 110, 0)
Stop
}
}

actor FrozenWind
{
PROJECTILE
+RIPPER
Damage 1
Speed 21
Radius 12
renderstyle add
damagetype "SjasScream"
Obituary "%o got the chills from %k's frozen wind."
alpha 0.8
scale 2.0
Height 20
+DOOMBOUNCE
+BLOODLESSIMPACT
States
{
Spawn:
PLSS J 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS K 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS L 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS M 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS N 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS O 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS P 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS Q 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS R 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS S 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS T 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS U 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS V 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS W 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
PLSS X 5 A_FadeOut(0.05)
PLSS J 0 A_JumpIfInTargetInventory("GhoulAmmo",1,"Death")
stop
Death:
PLSS X 0
stop
}
}

