actor Choke : PlayerPawn
{
	Health 150
	Mass 500
	limitedtoteam 1
	player.forwardmove 1.38
	player.sidemove 1.0
	Player.Maxhealth 150
	Player.DisplayName "Choke"
	player.startitem "Choketeeth"
	player.startitem "AchievementID", 1
	player.crouchsprite "CHOK"
	player.scoreicon "CHOKEST"
	player.soundclass "choke"
	Player.ColorRange 112, 127
	+NOSKIN
	scale 0.8
	+NOICEDEATH
	+QUICKTORETALIATE
	
	States
	{
		Spawn:
			TNT1 A 0	
			CHOK A 1
			CHOK A 0 ACS_Execute(984,0)
			Goto Spawn+1
		See:
			CHOK ABAC 1
			Goto Spawn+1
		Missile:
			CHOK E 0 A_SetTranslucent(1.0, 0) 
			CHOK E 0 A_JumpIfInventory("Clip",2,"Vomit")
			CHOK E 0 A_JumpIfInventory("Clip",1,"Spit")
			CHOK EF 4 A_JumpIfInventory("Clip",1,"Spit")
			CHOK A 8
			Goto Spawn+1
		Spit:
			CHOK E 0 A_JumpIfInventory("Clip",2,"Vomit")
			CHOK E 5 A_JumpIfInventory("Clip",2,"Vomit")
			CHOK F 32 A_JumpIfInventory("Clip",2,"Vomit")
			CHOK G 33 A_JumpIfInventory("Clip",2,"Vomit")
			CHOK E 10 A_TakeInventory("Clip",99)
			Goto Spawn+1
		Vomit:
			CHOK F 10 A_TakeInventory("Clip",99)
			CHOK E 23 A_TakeInventory("Clip",99)
			CHOK E 10 A_TakeInventory("Clip",99)
			Goto Spawn+1
		Death:
		DeathAnim:
			CHOK E 0 A_SpawnItemEx("GIBBER", 0, 0, 0, 0, 0, 0, 0, 160, 0)
			CHOK E 0 A_PlaySoundEx("chokedeath", "Body")
			CHOK E 0 A_GiveToTarget("KilledChoke",1)
			CHOK E 0 A_GiveToTarget("KilledMe",1)
			CHOK HI 11
			CHOK E 0
			CHOK JK 11
			CHOK E 0
			CHOK Z -1
			Stop
		Death.GBTrap:
			Goto Death.Grenade
		Death.Grenade:
			CHOK E 0 A_GiveToTarget("KilledG",1)
			Goto DeathAnim
		Death.CrapPlasma:
			CHOK E 0 A_GiveToTarget("Unlock9",1)
			Goto DeathAnim
		Death.Creeper:
			CHOK A 0 A_PlaySoundEx("creeperattack", "Body")
			CHOK A 0 ACS_ExecuteAlways(997,0)
			CHOK A 0 A_GiveToTarget("Creeperheal",1)
			Goto DeathAnim
		Death.ice2:
			CHOK A 0 A_GiveToTarget("Killedice2",1)
			Goto DeathAnim
		Death.fire2:
			CHOK A 0 A_GiveToTarget("Killedfire2",1)
			Goto DeathAnim
		Death.Railgun:
			CHOK A 0 A_GiveToTarget("Killedlightning",1)
			Goto DeathAnim
	}
}

actor KilledChoke : KilledSjas
{
}

Actor Choketeeth : Weapon
{
Weapon.SlotNumber 1
inventory.icon "CHOKEATT"
   Weapon.SelectionOrder 350
   Inventory.PickupSound ""
   Inventory.PickupMessage ""
   Obituary "%o was munched by %k" 
   +WEAPON.MELEEWEAPON
   Weapon.Kickback 0
   Weapon.YAdjust 0
   +INVENTORY.UNDROPPABLE
   States
   {
   Spawn:
      CHOK L 1
      Stop
   Ready:
      CHOK L 1 A_WeaponReady
      Loop
   Deselect:
	  CHOK L 1 A_Lower
      Loop
   Select:
      CHOK L 1 A_Raise
      Goto Select
   Fire:
     CHOK L 0 A_JumpIfInventory("ChokeCancelCheck",1,"Cancel")
	 CHOK L 0 A_Playweaponsound("chokeattack")
	 CHOK L 1 A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-10, 55) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-20, 75) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-30, 95) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-40, 115) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-50, 135) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-40, 115) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-30, 95) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-20, 75) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 1 offset (-10, 55) A_SpawnItemEx("ChokeAttack", 0, 0 ,28, momx, momy, 0, 0, 40)
	 CHOK L 5 offset (0, 32)
	 goto Ready
   Altfire:
     CHOK L 0 A_JumpIfInventory("ChokeCancelCheck",1,"Cancel")
     CHOK L 0 A_GiveInventory("Clip",1)
	 CHOK L 0 ACS_ExecuteAlways(983,0)//SetPlayerProperty(0, 1, 0)
	 CHOK L 0 A_PlayWeaponSound("chokeswallow")
	 CHOK L 2 offset (0, 35)
	 CHOK L 2 offset (0, 30)
	 CHOK L 2 offset (0, 25)
	 CHOK L 2 offset (0, 20)
	 CHOK L 2 offset (0, 15)
	 CHOK L 2 offset (0, 10)
	 CHOK L 25 offset (0, 5) A_GiveInventory("ChokeCancelCheck",1)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (5, 15) 
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (10, 20)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (5, 25)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (0, 30) 
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-5, 25)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-10, 20)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-5, 15)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (0, 10)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (0, 5) 
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (5, 15) 
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (10, 20)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (5, 25)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (0, 30)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-5, 25)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-10, 20) 
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (-5, 15)
	 CHOK L 0 A_WeaponReady
	 CHOK L 2 offset (0, 10)
	 CHOK L 0 offset (0, 5) A_FireCustomMissile("Chokeball", 0, 0 ,0)
	 CHOK L 1 offset (0, 55) A_PlayWeaponSound("chokespit")
	 CHOK L 1 offset (0, 75)
	 CHOK L 1 offset (0, 95)
	 CHOK L 1 offset (0, 115)
	 CHOK L 1 offset (0, 135) 
	 CHOK L 1 offset (0, 115)
	 CHOK L 1 offset (0, 95)
	 CHOK L 1 offset (0, 75)
	 CHOK L 1 offset (0, 55)
	 CHOK L 10 offset (0, 32) A_TakeInventory("ChokeCancelCheck",999)
	 CHOK L 15 offset (0, 32) ACS_ExecuteAlways(982,0)//SetPlayerProperty(0, 0, 0)
	 Goto Ready
    Cancel:
	 CHOK L 0 A_GiveInventory("Clip",1)
	 CHOK L 0 A_TakeInventory("ChokeCancelCheck",999)
	 CHOK L 0 A_PlayWeaponSound("chokevomit")
	 CHOK L 0 offset (0, 55) A_FireCustomMissile("VomitFake",0,0,0,0)
	 CHOK L 1 offset (0, 55)
	 CHOK L 1 offset (0, 60)
	 CHOK L 1 offset (0, 65)
	 CHOK L 0 ACS_ExecuteAlways(982,0)
	 CHOK L 1 offset (0, 70)
	 CHOK L 1 offset (0, 75)
	 CHOK L 1 offset (0, 80) 
	 CHOK L 1 offset (0, 85) 
	 CHOK L 1 offset (0, 90) 
	 CHOK LLLLLLLLLLLLLLLLLLLLLLLLL 1 offset (0, 95) A_FireCustomMissile("ChokeVomit",random(-20,20),0,0,3)
	 CHOK L 10 offset (0, 95) 
	 CHOK L 2 offset (0, 90) 
	 CHOK L 2 offset (0, 85)
	 CHOK L 2 offset (0, 80) 
	 CHOK L 2 offset (0, 75)
	 CHOK L 2 offset (0, 70)
	 CHOK L 2 offset (0, 65)
	 CHOK L 2 offset (0, 60)
	 CHOK L 2 offset (0, 55)
	 CHOK L 2 offset (0, 32)
	 Goto Ready
   }
}

actor ChokeVomit
{
Speed 1
height 2
damage (3)
radius 2
scale 0.2
renderstyle translucent
alpha 0.8
PROJECTILE
+RIPPER
-NOGRAVITY
+BLOODLESSIMPACT
States
{
Spawn:
VFX6 A 0
VFX6 A 5 A_Recoil(random(-12,-22))
VFX6 ABCDE 5
stop
}
}

actor VomitFake
{
PROJECTILE
-SOLID
+NOGRAVITY
States
{
Spawn:
TNT1 A 1
stop
}
}

ACTOR ChokeCancelCheck : Inventory
{
   Inventory.Amount 1
   Inventory.MaxAmount 1
}

Actor ChokeAttack
{
//PROJECTILE
+NOGRAVITY
+THRUGHOST
+FORCERADIUSDMG
+NOTELEPORT
Speed 25
Radius 2
Height 2
Obituary "%o was munched by %k." 
damagetype "Choke"
damage 0
+RIPPER
States{
Spawn:
TNT1 A 0
TNT1 A 1 A_Recoil(-25)
Goto Death
Death:
	TNT1 A 1 A_Explode(9, 56, 0)
Stop
}
}

actor Chokeball
{
PROJECTILE
Speed 50
-NOGRAVITY
Scale 0.8
+LOWGRAVITY
decal "BloodSplat"
damagetype "SjasScream"
Obituary "%o was stained by %k's blood ball." 
Damage 3
Height 5
Radius 6
reactiontime 8
States
{
Spawn:
BLBA AAAA 1 A_SpawnItem("NashGore_FlyingBlood",0,0,0,1)
BLBA A 0 A_GiveInventory("ChokeDist",1)
BLBA BBBB 1 A_SpawnItem("NashGore_FlyingBlood",0,0,0,1)
BLBA A 0 A_GiveInventory("ChokeDist",1)
BLBA CCCC 1 A_SpawnItem("NashGore_FlyingBlood",0,0,0,1)
BLBA A 0 A_GiveInventory("ChokeDist",1)
BLBA DDDD 1 A_SpawnItem("NashGore_FlyingBlood",0,0,0,1)
BLBA A 0 A_GiveInventory("ChokeDist",1)
BLBA A 0 A_CountDown
Loop
Death:
VFX6 A 0 A_JumpIfInventory("ChokeDist",11,"Far")
VFX6 A 0 A_Stop
VFX6 A 0 A_ChangeFlag("NOGRAVITY",1)
VFX6 A 0 A_Explode(125, 88, 0)
NULL A 0 A_PlaySound("gibbage/xsplat")
NULL A 0 A_SpawnItemEx ("GIB1", 1, 1, random(8,32), random(0,5), random(0,5), 0, random(0,360), 160, 0)
NULL A 0 A_SpawnItemEx ("GIB2", 1, 1, random(8,32), random(0,5), random(0,5), 0, random(0,360), 160, 0)
NULL A 0 A_SpawnItemEx ("GIB3", 1, 1, random(8,32), random(0,5), random(0,5), 0, random(0,360), 160, 0)
NULL A 0 A_SpawnItemEx ("GIB4", 1, 1, random(8,32), random(0,5), random(0,5), 0, random(0,360), 160, 0)
NULL A 0 A_SpawnItemEx ("GIB5", 1, 1, random(8,32), random(0,5), random(0,5), 0, random(0,360), 160, 0)
VFX6 A 3 A_FadeOut(0.08)
VFX6 A 0 A_TakeFromTarget("ChokeDist",999)
VFX6 BCDE 3 A_FadeOut(0.08)
Stop
Far:
VFX6 A 0 A_GiveToTarget("ChokeDist",1)
Goto Death+1
}
}

actor ChokeDist : KilledSjas
{
inventory.amount 1
inventory.maxamount 99999
}

actor HealDelay : PowerDamage
{
damagefactor "nothing", 1.0
powerup.duration 8
Inventory.MaxAmount 1
//inventory.icon "JMORPH"
}