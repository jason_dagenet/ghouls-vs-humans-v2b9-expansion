actor Cyborg : PlayerPawn
{
Health 100
Radius 16
Height 56
+NOICEDEATH
+NODAMAGETHRUST
+GHOST
limitedtoteam 0
mass 500
bloodtype "BulletPuff"
scale 0.92
PainChance 256
player.forwardmove 0.8
player.sidemove 0.8
player.soundclass "cyborg"
player.viewheight 48
Player.ColorRange 112, 127
damagefactor "Creepstun", 3.2
DamageFactor "GBTrap", 0.0
+NOSKIN
Player.DisplayName "cyborg"
Player.StartItem "CyborgPlasma"
Player.StartItem "CyborgJetpackItem"
Player.StartItem "CyborgCharge", 100
Player.StartItem "CyborgFuel", 150
player.scoreicon "CYBORGST"
+QUICKTORETALIATE
States
{
Spawn:
ROB1 D 0 SetPlayerProperty(0, 0, 3)
ROB1 D 15
ROB1 D 0 A_JumpIf (momz!=0, "Spawn")
ROB1 D 0 A_JumpIfInventory("CyborgCharge", 100, "Spawn")
ROB1 D 0 ACS_ExecuteAlways(996,0)
ROB1 D 0 //A_PlaySoundEx("weapons/cyborgcharge", "Body")
ROB1 D 0 A_GiveInventory("CyborgCharge",5)
Loop
ROB1 D 1
Loop
See:
ROB1 A 0 SetPlayerProperty(0, 0, 3)
ROB1 EEBB 3
ROB1 B 0 A_SpawnItemEx ("CyborgStomp", 0, 0, 0, 0, 0, -20, 0,0, 0) 
ROB1 CCDD 3
ROB1 B 0 A_SpawnItemEx ("CyborgStomp", 0, 0, 0, 0, 0, -20, 0,0, 0) 
Goto Spawn+7
Missile:
ROB1 G 18
Goto Spawn
Pain.Choke:
	TNT1 A 0
	TNT1 A 0 ACS_NamedExecuteAlways("SC_CHOKEHEALING")
	Goto Pain
Pain.Creepstun:
	TNT1 A 0
	TNT1 A 0 ACS_ExecuteAlways(986, 0)
	Goto Pain
Pain.CreeperOrbHeal:
	TNT1 A 0
	TNT1 A 0 ACS_NamedExecuteAlways("SC_CREEPER_ORBHEAL", 0)
	Goto Pain
Pain:
ROB1 H 4 A_Pain
Goto Spawn
Death.Creepstun:
ROB1 A 0 A_GiveToTarget("CreepGotya",1)
goto DeathAnim
Death.SjasScream:
ROB1 D 0 A_GiveToTarget("KilledJ",1)
Goto DeathAnim
Death.Choke:
Goto Death.Jitter+1
Death:
DeathAnim:
ROB1 A 0 A_TakeInventory("KilledCount",999)
ROB1 A 0 A_ChangeFlag("LOWGRAVITY",0)
ROB1 D 0 A_GiveToTarget("KilledMe",1)
ROB1 D 0 A_GiveToTarget("KilledCyborg",1)
ROB1 A 0 SetPlayerProperty(0, 0, 3)
ROB1 J 6
ROB1 K 6 A_PlayerScream
ROB1 L 5
ROB1 M 5 A_NoBlocking
ROB1 NOP 5
ROB1 Q 6
ROB1 R -1
Stop
Death.Creeper:
ROB1 A 0 A_PlaySoundEx("creeperattack", "Body")
ROB1 A 0 ACS_ExecuteAlways(997,0)
ROB1 A 0 A_GiveToTarget("Creeperheal",1)
Goto DeathAnim
Death.Creeper2:
	ROB1 A 0 A_PlaySoundEx("creeperattack", "Body")
	ROB1 A 0 ACS_ExecuteAlways(997,0)
	Goto DeathAnim
Death.Jitter:
TNT1 A 0 A_GiveToTarget("Health", 25)
ROB1 D 0 A_GiveToTarget("Crunched",1)
ROB1 D 0 A_GiveToTarget("KilledMe",1)
ROB1 D 0 A_GiveToTarget("KilledCyborg",1)
ROB1 H 1 A_PlaySound("weapons/grenadeexplode")
NULL A 0 A_SkullPop("Custom_BloodySkull3")
NULL A 0 A_NoBlocking 
// Unleash The Gibs!
NULL A 0 A_SpawnItemEx("CYBGIBBER", 0, 0, 0, 0, 0, 0, 0, 161, 0)
NULL A 1 A_SpawnItemEx("GrenadeExplosion", 0, 0, 0, 0, 0, 0, 0, 160, 0)
NULL A -1
Stop
}
}

actor CyborgPlasma : Weapon
{
Weapon.SelectionOrder 100
Weapon.AmmoUse 1
Weapon.SlotNumber 1
Weapon.AmmoUse2 2
Weapon.AmmoGive 50
Weapon.AmmoType "CyborgCharge"
Weapon.AmmoType2 "CyborgFuel"
inventory.icon "CYBWEP"
Inventory.PickupMessage "You got the cyborg's arm!.. somehow?"
Obituary "%o was devastated by %k's plasma." 
+INVENTORY.UNDROPPABLE
+AMMO_OPTIONAL
+WEAPON.NOAUTOAIM
+WEAPON.ALT_AMMO_OPTIONAL
+DONTBLAST
scale 0.6
States
{
Spawn:
TNT1 A 1
stop
Ready:
SABS A 0 A_JumpIfInventory("CyborgDashcheck",1,"Dashcheck")
SABS A 1 A_WeaponReady
Loop
Select:
SABS A 1 A_Raise
Loop
Deselect:
SABS A 1 A_Lower
Loop
Fire:
SABS A 0 ACS_ExecuteAlways(999,0)
SABS A 1 A_JumpIfInventory("CyborgCharge", 1, "Normalshot")
Weakshot:
SABA A 1 SetPlayerProperty(0, 0, 3)
SABA B 3 A_FireCustomMissile("CyborgPlasmaBallWeak", 0, 0, 11 ,3 )
SABA C 2
SABA D 3 
SABS A 3 A_Refire
Goto Ready
Normalshot:
SABA A 1 SetPlayerProperty(0, 0, 3)
SABA B 2 A_FireCustomMissile("CyborgPlasmaBall", 0, 1, 11 ,3 )
SABA C 1
SABA D 1 
SABS A 1 A_Refire
Goto Ready
Altfire:
SABS A 0 A_JumpIfInventory("CyborgDashcheck",1,"Dashcheck")
SABS A 1 A_JumpIfInventory("CyborgFuel", 1, "Jetpack")
SABS A 0 ACS_ExecuteAlways(999,0)
SABS A 0 SetPlayerProperty(0, 0, 3)
SABS A 18 A_PlaySoundEx("weapons/cyborgfail","Item")
Goto Ready
Jetpack:
SABS A 0 ACS_ExecuteAlways(998,0)
SABS A 0 SetPlayerProperty(0, 1, 3)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("SmokeLarge", -32, 0, 20, 0, 0, 0, 0, 128, 0) 
SABS A 3 A_TakeInventory("CyborgFuel",1)
SABS A 0 A_ReFire
SABS AAAA 1 A_WeaponReady
SABS A 0 ACS_ExecuteAlways(999,0)
SABS A 0 SetPlayerProperty(0, 0, 3)
Goto Ready
Dashcheck:
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
SABS A 1 A_JumpIf(z-floorz>8,"Ready")
SABS A 0 A_JumpIfInventory("CreeperStunCheck",1,"Jetpack")
SABS A 1 A_JumpIfInventory("CyborgFuel", 4, "Dash")
Goto Ready
Dash:
SABS A 0 A_PlaySoundEx("weapons/cyborgdash","Item")
SABS A 0 SetPlayerProperty(0, 0, 3)
SABS A 0 A_Stop
SABS A 0 A_JumpIf(ACS_ExecuteWithResult(991,0)==1,"Dashforward")
SABS A 0 A_Stop
SABS A 0 A_JumpIf(ACS_ExecuteWithResult(991,0)==2,"Dashback")
SABS A 0 A_Stop
SABS A 0 A_JumpIf(ACS_ExecuteWithResult(991,0)==3,"Dashleft")
SABS A 0 A_Stop
SABS A 0 A_JumpIf(ACS_ExecuteWithResult(991,0)==4,"Dashright")
Goto Dashupward
Dashupward:
SABS A 0 SetPlayerProperty(0, 1, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",1)
SABS A 0 A_TakeInventory("CyborgFuel",4)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 3 A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 3 A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 3 A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 3 A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 SetPlayerProperty(0, 0, 0)
SABS A 5 A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_ChangeFlag("NODAMAGE",0)
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
Goto Ready
Dashforward:
SABS A 0 offset(5,32) SetPlayerProperty(0, 1, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",1)
SABS A 0 A_TakeInventory("CyborgFuel",4)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(10,36)
SABS A 1 offset(15,40)
SABS A 1 offset(20,44) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(25,48)
SABS A 1 offset(30,52)
SABS A 1 offset(35,56) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(40,60)
SABS A 1 offset(45,64)
SABS A 1 offset(50,68) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(55,72)
SABS A 1 offset(60,76)
SABS A 1 offset(65,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 SetPlayerProperty(0, 0, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",0)
SABS A 14 offset(65,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 1 offset(60,76)
SABS A 1 offset(55,72)
SABS A 1 offset(50,68) 
SABS A 1 offset(45,64)
SABS A 1 offset(40,60)
SABS A 1 offset(35,56) 
SABS A 1 offset(30,52)
SABS A 1 offset(25,48)
SABS A 1 offset(20,44) 
SABS A 1 offset(15,40)
SABS A 1 offset(10,36)
SABS A 1 offset(5,32)
SABS A 3 offset(0,32)
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
Goto Ready
Dashback:
SABS A 0 offset(-2,32) SetPlayerProperty(0, 1, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",1)
SABS A 0 A_TakeInventory("CyborgFuel",4)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-4,36)
SABS A 1 offset(-6,40)
SABS A 1 offset(-8,44) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-10,48)
SABS A 1 offset(-12,52)
SABS A 1 offset(-14,56) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-16,60)
SABS A 1 offset(-18,64)
SABS A 1 offset(-20,68) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-22,72)
SABS A 1 offset(-24,76)
SABS A 1 offset(-26,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 SetPlayerProperty(0, 0, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",0)
SABS A 14 offset(-26,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 1 offset(-24,76)
SABS A 1 offset(-22,72)
SABS A 1 offset(-20,68) 
SABS A 1 offset(-18,64)
SABS A 1 offset(-16,60)
SABS A 1 offset(-14,56) 
SABS A 1 offset(-12,52)
SABS A 1 offset(-10,48)
SABS A 1 offset(-8,44) 
SABS A 1 offset(-6,40)
SABS A 1 offset(-4,36)
SABS A 1 offset(-2,32)
SABS A 3 offset(0,32)
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
Goto Ready
Dashleft:
SABS A 0 offset(6,32) SetPlayerProperty(0, 1, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",1)
SABS A 0 A_TakeInventory("CyborgFuel",4)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(12,36)
SABS A 1 offset(18,40)
SABS A 1 offset(24,44) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(30,48)
SABS A 1 offset(36,52)
SABS A 1 offset(42,56) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(48,60)
SABS A 1 offset(54,64)
SABS A 1 offset(60,68) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(66,72)
SABS A 1 offset(72,76)
SABS A 1 offset(78,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 SetPlayerProperty(0, 0, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",0)
SABS A 14 offset(78,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 1 offset(72,76)
SABS A 1 offset(66,72)
SABS A 1 offset(60,68) 
SABS A 1 offset(54,64)
SABS A 1 offset(48,60)
SABS A 1 offset(42,56) 
SABS A 1 offset(36,52)
SABS A 1 offset(30,48)
SABS A 1 offset(24,44) 
SABS A 1 offset(18,40)
SABS A 1 offset(12,36)
SABS A 1 offset(6,32)
SABS A 3 offset(0,32)
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
Goto Ready
Dashright:
SABS A 0 offset(-4,32) SetPlayerProperty(0, 1, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",1)
SABS A 0 A_TakeInventory("CyborgFuel",4)
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-8,36)
SABS A 1 offset(-12,40)
SABS A 1 offset(-16,44) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-20,48)
SABS A 1 offset(-24,52)
SABS A 1 offset(-28,56) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-32,60)
SABS A 1 offset(-36,64)
SABS A 1 offset(-40,68) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 1 offset(-44,72)
SABS A 1 offset(-48,76)
SABS A 1 offset(-52,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 0 A_SpawnItemEx("Firey", -32, 0, 28, 0, 0, -1, 0, 128, 0) 
SABS A 0 SetPlayerProperty(0, 0, 0)
SABS A 0 A_ChangeFlag("NODAMAGE",0)
SABS A 14 offset(-52,80) A_SpawnItemEx("SmokeLarge", -32, 10, 8, 0, 0, 0, 0, 128, 0) 
SABS A 1 offset(-48,76)
SABS A 1 offset(-44,72)
SABS A 1 offset(-40,68) 
SABS A 1 offset(-36,64)
SABS A 1 offset(-32,60)
SABS A 1 offset(-28,56) 
SABS A 1 offset(-24,52)
SABS A 1 offset(-20,48)
SABS A 1 offset(-16,44) 
SABS A 1 offset(-12,40)
SABS A 1 offset(-8,36)
SABS A 1 offset(-4,32)
SABS A 3 offset(0,32)
SABS A 0 A_TakeInventory("CyborgDashcheck",1)
Goto Ready
}
}

actor CyborgJetpackItem : CustomInventory
{
inventory.amount 1
inventory.maxamount 1
+INVBAR
+INVENTORY.UNDROPPABLE
States
{
Spawn:
TNT1 A 0
stop
Use:
TNT1 A 0 A_GiveInventory("CyborgDashcheck",1)
fail
}
}

ACTOR CyborgCharge : Ammo
{
+INVENTORY.IGNORESKILL
  Inventory.PickupMessage "Cell charge!"
  Inventory.Amount 100
  Inventory.MaxAmount 100
  Ammo.BackpackAmount 100
  Ammo.BackpackMaxAmount 100
  Inventory.Icon "CELLA0"
  States
  {
  Spawn:
    CELL A -1
    Stop
  }
}

ACTOR CyborgFuel : Ammo
{
+INVENTORY.IGNORESKILL
  Inventory.PickupMessage "Fuel charge!"
  Inventory.Amount 150
  Inventory.MaxAmount 150
  Ammo.BackpackAmount 150
  Ammo.BackpackMaxAmount 150
  Inventory.Icon "FUELA0"
  States
  {
  Spawn:
    CELL A -1
    Stop
  }
}


actor CyborgDashcheck : Inventory
{
Inventory.Amount 1
Inventory.MaxAmount 1
  States
  {
  Spawn:
    CELL A -1
    Stop
  }
}


actor CyborgPlasmaBall
{
scale 0.8
renderstyle add
alpha 0.95
PROJECTILE
decal "plasmadecal"
damagetype "Plasma"
speed 37
radius 10
height 5
Damage 6
seesound "weapons/cyborgplasmafire"
deathsound "weapons/cyborgplasmahit"
States
{
Spawn:
SBLL ABCDEFGHIJ 1 bright
Loop
Death:
SBLL KLMNOPQRSTU 1 bright A_FadeOut(0.04)
Stop
}
}

actor CyborgPlasmaBallWeak : CyborgPlasmaBall
{
decal "plasmadecal3"
damagetype "CrapPlasma"
radius 5
height 3
Damage 2
Speed 35
scale 0.25
}

ACTOR SmokeLarge : BulletPuff
{
+CLIENTSIDEONLY 
	Health		1
	RenderStyle Translucent
	Alpha		0
	Scale		.3
	States
	{
	Spawn:
		NULL A 0
		NULL A 0 ThrustThingZ(0,1,1,0)
		Goto See
	See:
		NULL A 0 A_Jump(192,1)
		Goto See+7
		NULL A 0
		NULL A 0 A_Jump(128,1)
		Goto See+11
		NULL A 0
		NULL A 0 A_Jump(64,1)
		Goto See+15
		NULL A 0
		NULL A 0
		Goto See+19
		//-----------------------
		SMOK A 12 A_SetTranslucent(.15,0)
		SMOK A 12 A_SetTranslucent(.35,0)
		SMOK A 12 A_SetTranslucent(.5,0)
		SMOK A 1 A_Fadeout(.02)
		Wait
		SMOK B 12 A_SetTranslucent(.15,0)
		SMOK B 12 A_SetTranslucent(.35,0)
		SMOK B 12 A_SetTranslucent(.5,0)
		SMOK B 1 A_Fadeout(.025)
		Wait
		SMOK C 12 A_SetTranslucent(.15,0)
		SMOK C 12 A_SetTranslucent(.35,0)
		SMOK C 12 A_SetTranslucent(.5,0)
		SMOK C 1 A_Fadeout(.03)
		Wait
		SMOK D 12 A_SetTranslucent(.15,0)
		SMOK D 12 A_SetTranslucent(.35,0)
		SMOK D 12 A_SetTranslucent(.5,0)
		SMOK D 1 A_Fadeout(.035)
		Wait
	}
}

ACTOR Firey : BulletPuff
{
+CLIENTSIDEONLY
	Health		1
	RenderStyle Add
	Alpha		0
	Scale		.17
	Obituary "%o burned to death"
	States
	{
	Spawn:
		NULL A 0
		NULL A 0 A_PlaySound("weapons/cyborgjetpack")
		Goto See
	See:
		NULL A 0 A_Jump(170,1)
		Goto See+7
		NULL A 0
		NULL A 0 A_Jump(85,1)
		Goto See+11
		NULL A 0
		NULL A 0
		Goto See+15
		//-----------------------
		BFIR B 5 Bright //A_PlaySound("Barrel/Fire")
		BFIR A 5 Bright A_SetTranslucent(.4,1)
		BFIR A 5 Bright A_SetTranslucent(.8,1)
		BFIR A 1 Bright A_Fadeout(.06)
		Wait
		BFIR B 5 Bright //A_PlaySound("Barrel/Fire")
		BFIR B 5 Bright A_SetTranslucent(.4,1)
		BFIR B 5 Bright A_SetTranslucent(.8,1)
		BFIR B 1 Bright A_Fadeout(.08)
		Wait
		BFIR B 5 Bright //A_PlaySound("Barrel/Fire")
		BFIR C 5 Bright A_SetTranslucent(.4,1)
		BFIR C 5 Bright A_SetTranslucent(.8,1)
		BFIR C 1 Bright A_Fadeout(.10)
		Wait
	}
}