[IntroStrings]
"Target acquired."
"Arrived at destination."
"Scanning Environment... Enemies detected."

[FragStrings]
"Target exterminated. Continue scan."
"Acquiring new target..."
"Enemy destroyed."

[KilledStrings]
"Rebooting."
"Error."
"Malfunction."

[RareRoamingStrings]
"System check... All files OK."
"System check... All services enabled."
"System check... System speed at 127%"
"System check... No problems detected."
"System check... Temperature Normal."
"Keyboard not found, press F1 to resume."

[RoamingStrings]
"Following orders. Targets acquired."
"Enemies still remain. Scanning..."
"Weapons armed. Locating targets."
"Error: Targets still remain."

[EnragedStrings]
"Powerlevels increasing."
"Enemies WILL be eliminated."
"Extermination."

[DemoralizedStrings]
"Error. Error."
"Unknown status. Rebooting."
"Unknown error."

[WinStrings]
"Targets eliminated. Returning to base."
"Ghouls exterminated. Shut down."
"Enemies remaining: 0. Mission complete."

[LoseStrings]
"Self-destruct sequence... Activated."
"Mission failed. Reporting to superiors."
"/me shuts down permanently"
