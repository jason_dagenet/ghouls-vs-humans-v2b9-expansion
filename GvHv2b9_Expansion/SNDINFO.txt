// GHOUL SOUNDS
$random skullact {skull1 skull2 skull3 skull4}
skull1 skull1
skull2 skull2
skull3 skull3
skull4 skull4
skullattack skull5
skulldie skull6
skullchips	dsrocks
skulleating CRUNCHIN
$random jitpain {jitpain1 jitpain2 jitpain3 jitpain4}
jitpain1 jitpain1 
jitpain2 jitpain2 
jitpain3 jitpain3 
jitpain4 jitpain4 
creepgotya creepgot
creeperact creep1
creeperattack creep2
creeperpain creep3
creeperdie creep4
CreeperBall dscurse
CreeperBallD dscursed
sjasattack		sjasatta
sjasdeath		sjasdeat
sjaspain		sjaspain
sjasact			sjasact
sjasreflect		sjassee
ghoul3	ghoul3

// MISC

$random menu/quit1 { creeperattack sjasreflect skulldie skullattack creeperpain weapons/icearrow weapons/lightningarrow }
$random menu/quit2 { sjasact sjaspain sjasdeath skull4 CreeperBallD creeperdie weapons/bowfire }

AchievementUnlock aunlock

// WEAPON SOUNDS

weapons/chngun2fire1		dscgn2f1
//weapons/chngun2load1		dscgn2l1
weapons/combogun_click			dszip
weapons/combogun_open			dszopen
weapons/combogun_close			dszclose
weapons/combogun_light			torch2

$random weapons/arrowhit { weapons/arrowhit1 weapons/arrowhit2 weapons/arrowhit3 }
weapons/arrowhit1 DSBOWHI1
weapons/arrowhit2 DSBOWHI2
weapons/arrowhit3 DSBOWHI3
weapons/bowup			dsbowup
weapons/bowdraw			dsbowdra
weapons/bowabort 		dsbowab
weapons/bowfire			dsbowfi
weapons/bowpowerup		dspowhun
weapons/bowfswitch		dsflamer
weapons/bowiswitch		dscrystr
weapons/bowlswitch		dslitchr
weapons/arrowexplode	dsfirard
weapons/flamearrow		dsleechb
weapons/icearrow		dsicebol
weapons/arrowshatter	dscrysbk
weapons/lightningarrow	dscelfir
weapons/arrowno		dsbowno
weapons/firespin		firpower
weapons/icespike		icepower
weapons/lightfall		litpower
weapons/lighthit		litfloor

weapons/creepercharge			dsblur

weapons/cyborgplasmafire	DSBLSHT3
weapons/cyborgplasmahit		DSSONHIT
weapons/cyborgcharge		DSRAICRG
weapons/cyborgfail			BLSHELL
weapons/cyborgjetpack		DSFLBURN
weapons/cyborgsuperjet		SUPERJ
weapons/cyborgno			NOSUPER
weapons/cyborgdash			CYBDASH
Cyborg2Flamer/Start			cfl2_up
Cyborg2Flamer/Fire			cfl2_fir
Cyborg2Flamer/Stop			cfl2_dow

weapons/grenadeexplode		DSGRBOOM
weapons/grenadethrow		DSGRTOSS
weapons/grenadebounce		DSGRDROP
weapons/grenadepin			DSGRPIN
weapons/grenadeno		DSFUMBLE
weapons/grenademax		GRENMAX

$random puff/ric      { puff/ric1  puff/ric2  puff/ric3 } 
puff/ric1         dsric1 
puff/ric2         dsric2 
puff/ric3         dsric3

// CLASS SOUNDS
CyborgStomp DSRB2PN
newteleportfog	DSNEWFOG

$random floor/hardx		{ floor/hardx01  floor/hardx02 floor/hardx03 floor/hardx04  floor/hardx05 floor/hardx06 }
floor/hardx01	dshard1
floor/hardx02	dshard2
floor/hardx03	dshard3
floor/hardx04	dshard4
floor/hardx05	dshard5
floor/hardx06	dshard6

$playersound	sjas	male	*death		dsnone
$playersound	sjas	male	*xdeath		dsnone
$playersound	sjas	male	*gibbed		dsnone
$playersound	sjas	male	*pain100	dsnone
$playersound	sjas	male	*pain75		dsnone
$playersound	sjas	male	*pain50		dsnone
$playersound	sjas	male	*pain25		dsnone
$playersound	sjas	male	*grunt		dsnone
$playersound	sjas	male	*land		dsnone
$playersound	sjas	male	*jump		dsnone
$playersound	sjas	male	*fist		dsnone
$playersound	sjas	male	*usefail	dsnone
$playersound	sjas	male	*taunt		sjastau

$playersound	sjas	female	*death		dsnone
$playersound	sjas	female	*xdeath		dsnone
$playersound	sjas	female	*gibbed		dsnone
$playersound	sjas	female	*pain100	dsnone
$playersound	sjas	female	*pain75		dsnone
$playersound	sjas	female	*pain50		dsnone
$playersound	sjas	female	*pain25		dsnone
$playersound	sjas	female	*grunt		dsnone
$playersound	sjas	female	*land		dsnone
$playersound	sjas	female	*jump		dsnone
$playersound	sjas	female	*fist		dsnone
$playersound	sjas	female	*usefail	dsnone
$playersound	sjas	female	*taunt		sjastau

$playersound	sjas	other	*death		dsnone
$playersound	sjas	other	*xdeath		dsnone
$playersound	sjas	other	*gibbed		dsnone
$playersound	sjas	other	*pain100	dsnone
$playersound	sjas	other	*pain75		dsnone
$playersound	sjas	other	*pain50		dsnone
$playersound	sjas	other	*pain25		dsnone
$playersound	sjas	other	*pain75		dsnone
$playersound	sjas	other	*grunt		dsnone
$playersound	sjas	other	*land		dsnone
$playersound	sjas	other	*jump		dsnone
$playersound	sjas	other	*fist		dsnone
$playersound	sjas	other	*usefail	dsnone
$playersound	sjas	other	*taunt		sjastau

$playeralias sjas male *taunt sjastaunt 
$playeralias sjas female *taunt sjastaunt
$playeralias sjas other *taunt sjastaunt
sjastaunt sjastau
$limit sjastaunt 1

$playersound	creeper	male	*death		dsnone
$playersound	creeper	male	*xdeath		dsnone
$playersound	creeper	male	*gibbed		dsnone
$playersound	creeper	male	*pain100	dsnone
$playersound	creeper	male	*pain75		dsnone
$playersound	creeper	male	*pain50		dsnone
$playersound	creeper	male	*pain25		dsnone
$playersound	creeper	male	*grunt		creep1
$playersound	creeper	male	*land		creep1
$playersound	creeper	male	*jump		creep1
$playersound	creeper	male	*fist		dsnone
$playersound	creeper	male	*usefail	creep1
$playersound	creeper	male	*taunt		creeptau

$playersound	creeper	female	*death		dsnone
$playersound	creeper	female	*xdeath		dsnone
$playersound	creeper	female	*gibbed		dsnone
$playersound	creeper	female	*pain100	dsnone
$playersound	creeper	female	*pain75		dsnone
$playersound	creeper	female	*pain50		dsnone
$playersound	creeper	female	*pain25		dsnone
$playersound	creeper	female	*grunt		creep1
$playersound	creeper	female	*land		creep1
$playersound	creeper	female	*jump		creep1
$playersound	creeper	female	*fist		dsnone
$playersound	creeper	female	*usefail	dsnone
$playersound	creeper	female	*taunt		creeptau

$playersound	creeper	other	*death		dsnone
$playersound	creeper	other	*xdeath		dsnone
$playersound	creeper	other	*gibbed		dsnone
$playersound	creeper	other	*pain100	dsnone
$playersound	creeper	other	*pain75		dsnone
$playersound	creeper	other	*pain50		dsnone
$playersound	creeper	other	*pain25		dsnone
$playersound	creeper	other	*pain75		dsnone
$playersound	creeper	other	*grunt		creep1
$playersound	creeper	other	*land		creep1
$playersound	creeper	other	*jump		creep1
$playersound	creeper	other	*fist		dsnone
$playersound	creeper	other	*usefail	dsnone
$playersound	creeper	other	*taunt		creeptau

$playeralias creeper male *taunt creeptaunt 
$playeralias creeper female *taunt creeptaunt
$playeralias creeper other *taunt creeptaunt
creeptaunt creeptau
$limit creeptaunt 1

$playersound	jskull	male	*death		dsnone
$playersound	jskull	male	*xdeath		dsnone
$playersound	jskull	male	*gibbed		dsnone
$playersound	jskull	male	*pain100	dsnone
$playersound	jskull	male	*pain75		dsnone
$playersound	jskull	male	*pain50		dsnone
$playersound	jskull	male	*pain25		dsnone
$playersound	jskull	male	*grunt		dsnone
$playersound	jskull	male	*land		dsnone
$playersound	jskull	male	*jump		skullact
$playersound	jskull	male	*fist		dsnone
$playersound	jskull	male	*usefail	dsnone
$playersound	jskull	male	*taunt		skultau

$playersound	jskull	female	*death		dsnone
$playersound	jskull	female	*xdeath		dsnone
$playersound	jskull	female	*gibbed		dsnone
$playersound	jskull	female	*pain100	dsnone
$playersound	jskull	female	*pain75		dsnone
$playersound	jskull	female	*pain50		dsnone
$playersound	jskull	female	*pain25		dsnone
$playersound	jskull	female	*grunt		dsnone
$playersound	jskull	female	*land		dsnone
$playersound	jskull	female	*jump		skullact
$playersound	jskull	female	*fist		dsnone
$playersound	jskull	female	*usefail	dsnone
$playersound	jskull	female	*taunt		skultau

$playersound	jskull	other	*death		dsnone
$playersound	jskull	other	*xdeath		dsnone
$playersound	jskull	other	*gibbed		dsnone
$playersound	jskull	other	*pain100	dsnone
$playersound	jskull	other	*pain75		dsnone
$playersound	jskull	other	*pain50		dsnone
$playersound	jskull	other	*pain25		dsnone
$playersound	jskull	other	*pain75		dsnone
$playersound	jskull	other	*grunt		dsnone
$playersound	jskull	other	*land		dsnone
$playersound	jskull	other	*jump		skullact
$playersound	jskull	other	*fist		dsnone
$playersound	jskull	other	*usefail	dsnone
$playersound	jskull	other	*taunt		skultau

$playeralias jskull male *taunt skultaunt 
$playeralias jskull female *taunt skultaunt
$playeralias jskull other *taunt skultaunt
skultaunt skultau
$limit skultaunt 1

$playersound	cyborg	male	*death		cybdeath
$playersound	cyborg	male	*xdeath		cybdeath
$playersound	cyborg	male	*gibbed		cybdeath
$playersound	cyborg	male	*pain100	cybpain
$playersound	cyborg	male	*pain75		cybpain
$playersound	cyborg	male	*pain50		cybpain
$playersound	cyborg	male	*pain25		cybpain
$playersound	cyborg	male	*grunt		cyboof
$playersound	cyborg	male	*land		cyboof
$playersound	cyborg	male	*jump		cybjump
$playersound	cyborg	male	*fist		dsnone
$playersound	cyborg	male	*usefail	cybnoway
$playersound	cyborg	male	*taunt		cybtaunt

$playersound	cyborg	female	*death		dsfcdeth
$playersound	cyborg	female	*xdeath		dsfcdeth
$playersound	cyborg	female	*gibbed		dsfcdeth
$playersound	cyborg	female	*pain100	dsfcpain
$playersound	cyborg	female	*pain75		dsfcpain
$playersound	cyborg	female	*pain50		dsfcpain
$playersound	cyborg	female	*pain25		dsfcpain
$playersound	cyborg	female	*grunt		dsfcland
$playersound	cyborg	female	*land		dsfcland
$playersound	cyborg	female	*jump		dsfcjump
$playersound	cyborg	female	*fist		dsnone
$playersound	cyborg	female	*usefail	dsfcnowa
$playersound	cyborg	female	*taunt		dsfctaun

$playersound	cyborg	other	*death		cybdeth
$playersound	cyborg	other	*xdeath		cybdeth
$playersound	cyborg	other	*gibbed		cybdeth
$playersound	cyborg	other	*pain100	cybpain
$playersound	cyborg	other	*pain75		cybpain
$playersound	cyborg	other	*pain50		cybpain
$playersound	cyborg	other	*pain25		cybpain
$playersound	cyborg	other	*grunt		cyboof
$playersound	cyborg	other	*land		cyboof
$playersound	cyborg	other	*jump		cybjump
$playersound	cyborg	other	*fist		dsnone
$playersound	cyborg	other	*usefail	cybnoway
$playersound	cyborg	other	*taunt		cybtaunt

$playeralias cyborg male *taunt cybtaunt 
$playeralias cyborg female *taunt cybtaunt2
$playeralias cyborg other *taunt cybtaunt
cybtaunt cybtaunt
$limit cybtaunt 1
cybtaunt2 dsfctaun
$limit cybtaunt2 1

$playersound	hunter	male	*death		huntdth
$playersound	hunter	male	*xdeath		huntdth
$playersound	hunter	male	*gibbed		huntdth
$playersound	hunter	male	*pain100	huntpain
$playersound	hunter	male	*pain75		huntpain
$playersound	hunter	male	*pain50		huntpain
$playersound	hunter	male	*pain25		huntpain
$playersound	hunter	male	*grunt		huntoof
$playersound	hunter	male	*land		huntland
$playersound	hunter	male	*jump		dsnone
$playersound	hunter	male	*fist		dsnone
$playersound	hunter	male	*usefail	huntoof
$playersound	hunter	male	*taunt		hunttaun

$playersound	hunter	female	*death		dsfhdeth
$playersound	hunter	female	*xdeath		dsfhdeth
$playersound	hunter	female	*gibbed		dsfhdeth
$playersound	hunter	female	*pain100	dsfhpain
$playersound	hunter	female	*pain75		dsfhpain
$playersound	hunter	female	*pain50		dsfhpain
$playersound	hunter	female	*pain25		dsfhpain
$playersound	hunter	female	*grunt		dsfhnowa
$playersound	hunter	female	*land		huntland
$playersound	hunter	female	*jump		dsnone
$playersound	hunter	female	*fist		dsnone
$playersound	hunter	female	*usefail	dsfhnowa
$playersound	hunter	female	*taunt		dsfhtaun

$playersound	hunter	other	*death		huntdth
$playersound	hunter	other	*xdeath		huntdth
$playersound	hunter	other	*gibbed		huntdth
$playersound	hunter	other	*pain100	huntpain
$playersound	hunter	other	*pain75		huntpain
$playersound	hunter	other	*pain50		huntpain
$playersound	hunter	other	*pain25		huntpain
$playersound	hunter	other	*grunt		huntoof
$playersound	hunter	other	*land		huntland
$playersound	hunter	other	*jump		dsnone
$playersound	hunter	other	*fist		dsnone
$playersound	hunter	other	*usefail	huntoof
$playersound	hunter	other	*taunt		hunttaun

$playeralias hunter male *taunt hunttaunt 
$playeralias hunter female *taunt hunttaunt2
$playeralias hunter other *taunt hunttaunt

hunttaunt hunttaun
hunttaunt2 dsfhtaun
$limit hunttaunt 1
$limit hunttaunt2 1

$playersound	marine	female	*death		dscrdeth
$playersound	marine	female	*xdeath		dscrdeth
$playersound	marine	female	*gibbed		dscrdeth
$playersound	marine	female	*pain100 	dscrpain
$playersound	marine	female	*pain75 	dscrpain
$playersound	marine	female	*pain50 	dscrpain
$playersound	marine	female	*pain25 	dscrpain
$playersound	marine	female	*grunt		dscrnway
$playersound	marine	female	*land		dscroof
$playersound	marine	female	*fist		dsnone
$playersound	marine	female	*usefail	dscrnway
$playersound	marine	female	*taunt		dscrtant

$playeralias marine male *taunt playertaunt 
$playeralias marine female *taunt playertaunt2
$playeralias marine other *taunt playertaunt
playertaunt dstaunt
playertaunt2 dscrtant
$limit playertaunt 1
$limit playertaunt2 1



// GIB SOUNDS

$random gibbage/xsplat			{gibbage/splat1 gibbage/splat2 gibbage/splat3 gibbage/splat4}
gibbage/splat1				splat1
gibbage/splat2				splat2
gibbage/splat3				splat3
gibbage/splat4				splat4

$random gibbage/blood		{gibbage/blood1 gibbage/blood2 gibbage/blood3 gibbage/blood4}
gibbage/blood1				blood1
gibbage/blood2				blood2
gibbage/blood3				blood3
gibbage/blood4				blood4

gibbage/null				dsnull

// Gib goes Splat
$random gibbage/xgib			{gibbage/gib1 gibbage/gib2 gibbage/gib3}
gibbage/gib1				gib1
gibbage/gib2				gib2
gibbage/gib3				gib3

$random gibbage/xgibst			{gibbage/git1 gibbage/git2 gibbage/git3}
gibbage/git1				git1
gibbage/git2				git2
gibbage/git3				git3

// Body Part goes Splat
$random gibbage/xpart			{gibbage/part1 gibbage/part2 gibbage/part3}
gibbage/part1				part1
gibbage/part2				part2
gibbage/part3				part3

// Cyborg Gibs

$random gibbage/cyborg			{gibbage/junk1 gibbage/junk2 gibbage/junk3 gibbage/junk4}
gibbage/junk1				jnk1
gibbage/junk2				jnk2
gibbage/junk3				jnk3
gibbage/junk4				jnk4


// EW SOUNDS

chokedeath	dschodth
chokeattack	dschoatk
chokeswallow	dschoswa
chokespit	dschospi
chokevomit	chokesik

$playersound	choke	male	*death		dsnone
$playersound	choke	male	*xdeath		dsnone
$playersound	choke	male	*gibbed		dsnone
$playersound	choke	male	*pain100	dschopai
$playersound	choke	male	*pain75		dschopai
$playersound	choke	male	*pain50		dschopai
$playersound	choke	male	*pain25		dschopai
$playersound	choke	male	*grunt		dschoact
$playersound	choke	male	*land		dschoact
$playersound	choke	male	*jump		dsnone
$playersound	choke	male	*fist		dsnone
$playersound	choke	male	*usefail	dsnone
$playersound	choke	male	*taunt		dschotau

$playersound	choke	female	*death		dsnone
$playersound	choke	female	*xdeath		dsnone
$playersound	choke	female	*gibbed		dsnone
$playersound	choke	female	*pain100	dschopai
$playersound	choke	female	*pain75		dschopai
$playersound	choke	female	*pain50		dschopai
$playersound	choke	female	*pain25		dschopai
$playersound	choke	female	*grunt		dschoact
$playersound	choke	female	*land		dschoact
$playersound	choke	female	*jump		dsnone
$playersound	choke	female	*fist		dsnone
$playersound	choke	female	*usefail	dsnone
$playersound	choke	female	*taunt		dschotau

$playersound	choke	other	*death		dsnone
$playersound	choke	other	*xdeath		dsnone
$playersound	choke	other	*gibbed		dsnone
$playersound	choke	other	*pain100	dschopai
$playersound	choke	other	*pain75		dschopai
$playersound	choke	other	*pain50		dschopai
$playersound	choke	other	*pain25		dschopai
$playersound	choke	other	*grunt		dschoact
$playersound	choke	other	*land		dschoact
$playersound	choke	other	*jump		dsnone
$playersound	choke	other	*fist		dsnone
$playersound	choke	other	*usefail	dsnone
$playersound	choke	other	*taunt		dschotau

$playeralias choke male *taunt choketaunt
$playeralias choke female *taunt choketaunt
$playeralias choke other *taunt choketaunt
choketaunt dschotau
$limit choketaunt 1

weapons/protonpackfire		DSBEAMS	
weapons/trapcharge		DSGBCRG
weapons/trap		DSBFFF1
weapons/trapdetonate	DSTRAPD
weapons/traphit		DSTRAPH
weapons/trapzip		ZAP2
pack/heatup	dspack2
pack/change1	dspack3
pack/change2	dspack4
slime1		dsslime1	
echoshot	dsgenpls
echobounce	dsphtxp2
boneshot DSFGFIRE
icerain	dsfrostd
gbsoul	dsgbso
gbupgrade	dsgbup
gbupgradefail	dsgberr

$playersound	peter	male	*death		peterdth
$playersound	peter	male	*xdeath		peterdth
$playersound	peter	male	*gibbed		peterdth
$playersound	peter	male	*pain100	peterpai
$playersound	peter	male	*pain75		peterpai
$playersound	peter	male	*pain50		peterpai
$playersound	peter	male	*pain25		peterpai
$playersound	peter	male	*grunt		huntland
$playersound	peter	male	*land		huntland
$playersound	peter	male	*jump		dsnone
$playersound	peter	male	*fist		dsnone
$playersound	peter	male	*usefail	dsnoway
$playersound	peter	male	*taunt		petertau

$playersound	peter	female	*dea4th		peterdth
$playersound	peter	female	*xdeath		peterdth
$playersound	peter	female	*gibbed		peterdth
$playersound	peter	female	*pain100	peterpai
$playersound	peter	female	*pain75		peterpai
$playersound	peter	female	*pain50		peterpai
$playersound	peter	female	*pain25		peterpai
$playersound	peter	female	*grunt		huntland
$playersound	peter	female	*land		huntland
$playersound	peter	female	*jump		dsnone
$playersound	peter	female	*fist		dsnone
$playersound	peter	female	*usefail	dsfnoway
$playersound	peter	female	*taunt		petertau

$playersound	peter	other	*death		peterdth
$playersound	peter	other	*xdeath		peterdth
$playersound	peter	other	*gibbed		peterdth
$playersound	peter	other	*pain100	peterpai
$playersound	peter	other	*pain75		peterpai
$playersound	peter	other	*pain50		peterpai
$playersound	peter	other	*pain25		peterpai
$playersound	peter	other	*grunt		huntland
$playersound	peter	other	*land		huntland
$playersound	peter	other	*jump		dsnone
$playersound	peter	other	*fist		dsnone
$playersound	peter	other	*usefail	dscnoway
$playersound	peter	other	*taunt		petertau

$playeralias peter male *taunt petertaunt 
$playeralias peter female *taunt petertaunt
$playeralias peter other *taunt petertaunt
petertaunt petertau
$limit petertaunt 1

$playersound	frost	male	*death		frostdie
$playersound	frost	male	*xdeath		dsnone
$playersound	frost	male	*gibbed		dsnone
$playersound	frost	male	*pain100	dsnone
$playersound	frost	male	*pain75		dsnone
$playersound	frost	male	*pain50		dsnone
$playersound	frost	male	*pain25		dsnone
$playersound	frost	male	*grunt		dsnone
$playersound	frost	male	*land		dsnone
$playersound	frost	male	*jump		dsnone
$playersound	frost	male	*fist		dsnone
$playersound	frost	male	*usefail	dsnone
$playersound	frost	male	*taunt		frosttaunt

$playersound	frost	female	*death		frostdie
$playersound	frost	female	*xdeath		dsnone
$playersound	frost	female	*gibbed		dsnone
$playersound	frost	female	*pain100	dsnone
$playersound	frost	female	*pain75		dsnone
$playersound	frost	female	*pain50		dsnone
$playersound	frost	female	*pain25		dsnone
$playersound	frost	female	*grunt		dsnone
$playersound	frost	female	*land		dsnone
$playersound	frost	female	*jump		dsnone
$playersound	frost	female	*fist		dsnone
$playersound	frost	female	*usefail	dsnone
$playersound	frost	female	*taunt		frosttaunt

$playersound	frost	other	*death		frostdie
$playersound	frost	other	*xdeath		dsnone
$playersound	frost	other	*gibbed		dsnone
$playersound	frost	other	*pain100	dsnone
$playersound	frost	other	*pain75		dsnone
$playersound	frost	other	*pain50		dsnone
$playersound	frost	other	*pain25		dsnone
$playersound	frost	other	*grunt		dsnone
$playersound	frost	other	*land		dsnone
$playersound	frost	other	*jump		dsnone
$playersound	frost	other	*fist		dsnone
$playersound	frost	other	*usefail	dsnone
$playersound	frost	other	*taunt		frosttaunt

$playeralias frost male *taunt frosttaunt
$playeralias frost female *taunt frosttaunt
$playeralias frost other *taunt frosttaunt
frosttaunt frosttau
$limit frosttaunt 1

FrostBreathStart	FROSTSTA
FrostBreathMid	FROSTMID
FrostBreathEnd	FROSTEND
FrostBreathFail	FROSTBFA
FrostPain		FROSTPAI
FrostChomp		FROSTCHO
FrostTeeth		SHARDS1B
FrostDeath		FROSTDIE
FrostBurst		DSRXPLOD

$random FrostTwinkle {FrostTwinkle1 FrostTwinkle2 FrostTwinkle3 FrostTwinkle4 FrostTwinkle5 FrostTwinkle6}
FrostTwinkle1		FROSTIN1
FrostTwinkle2		FROSTIN2
FrostTwinkle3		FROSTIN3
FrostTwinkle4		FROSTIN4
FrostTwinkle5		FROSTIN5
FrostTwinkle6		FROSTIN6

$playersound	engin	male	*death		engideat
$playersound	engin	male	*xdeath		engideat
$playersound	engin	male	*gibbed		engideat
$playersound	engin	male	*pain100	engipain
$playersound	engin	male	*pain75		engipain
$playersound	engin	male	*pain50		engipain
$playersound	engin	male	*pain25		engipain
$playersound	engin	male	*grunt		engiland
$playersound	engin	male	*land		engiland
$playersound	engin	male	*jump		dsnone
$playersound	engin	male	*fist		dsnone
$playersound	engin	male	*usefail	dsnone
$playersound	engin	male	*taunt		engitaun

$playersound	engin	female	*death		engideat
$playersound	engin	female	*xdeath		engideat
$playersound	engin	female	*gibbed		engideat
$playersound	engin	female	*pain100	engipain
$playersound	engin	female	*pain75		engipain
$playersound	engin	female	*pain50		engipain
$playersound	engin	female	*pain25		engipain
$playersound	engin	female	*grunt		engiland
$playersound	engin	female	*land		engiland
$playersound	engin	female	*jump		dsnone
$playersound	engin	female	*fist		dsnone
$playersound	engin	female	*usefail	dsnone
$playersound	engin	female	*taunt		engitaun

$playersound	engin	other	*death		engideat
$playersound	engin	other	*xdeath		engideat
$playersound	engin	other	*gibbed		engideat
$playersound	engin	other	*pain100	engipain
$playersound	engin	other	*pain75		engipain
$playersound	engin	other	*pain50		engipain
$playersound	engin	other	*pain25		engipain
$playersound	engin	other	*grunt		engiland
$playersound	engin	other	*land		engiland
$playersound	engin	other	*jump		dsnone
$playersound	engin	other	*fist		dsnone
$playersound	engin	other	*usefail	dsnone
$playersound	engin	other	*taunt		engitaun

$playeralias engin male *taunt engitaunt 
$playeralias engin female *taunt engitaunt
$playeralias engin other *taunt engitaunt
engitaunt engitaun
$limit engitaunt 1

EngineerNail	NAILFI
SawThrow		BLADELAU
SawHit		BLADEDTH
SawAir		BLADEAIR
SawClick	DSCLICK
SteamLaunch		STEAMFIR
SteamWhistle	TRAINW
SteamFire	STEAMFLY
Steam	STEAM
SteamHit			STEAMDIE